#include "Dataset.h"
#include <opencv2/opencv.hpp>

using Example = torch::data::Example<>;

Example CustomDataset::get(size_t index) {

	int imageWidth  = 256;
	int imageHeight = 384;


	std::string path = data[index].first.imagePath[0];

	//cout << "path = " << path << endl;
	ofImage img(path);
	auto mat = cv::Mat(img.getHeight(), img.getWidth(), CV_MAKETYPE(CV_8U, img.getPixels().getNumChannels()), img.getPixels().getData());
	assert(!mat.empty());

	cv::resize(mat, mat, cv::Size(imageWidth, imageHeight));
	cv::Mat tempImage;	
	mat.convertTo(tempImage, CV_32FC1, 1.f / 255);	

	auto imgTensor = torch::from_blob(tempImage.ptr<float>(), { imageWidth, imageHeight }, torch::kFloat);
	auto tdata = torch::cat({ imgTensor }).view({ 1, imageHeight, imageWidth });//.to(torch::kFloat);

	addImageToTensor(tdata, data[index].first.imagePath[1], 2, imageWidth, imageHeight);
	addImageToTensor(tdata, data[index].first.imagePath[2], 3, imageWidth, imageHeight);


	/// ������ �����:
	std::string pathMask = ofToDataPath(data[index].second.imagePath[0], true);
	//cout << "pathMask = " << pathMask << endl;
	ofImage imgMask(pathMask);
	auto matMask = cv::Mat(imgMask.getHeight(), imgMask.getWidth(), CV_MAKETYPE(CV_8U, imgMask.getPixels().getNumChannels()), imgMask.getPixels().getData());//cv::imread(path);
	assert(!matMask.empty());

	cv::resize(matMask, matMask, cv::Size(imageWidth, imageHeight));
	cv::Mat tempImageMask;
	matMask.convertTo(tempImageMask, CV_32FC1, 1.f / 255);
	auto maskTensor = torch::from_blob(tempImageMask.ptr<float>(), { imageWidth, imageHeight }, torch::kFloat);
	auto tlabel = torch::cat({ maskTensor }).view({ 1, imageHeight, imageWidth });//.to(torch::kFloat);

	if (options.num_classes > 1) {
		for (size_t k = 1; k < data[index].second.imagePath.size(); k++)		
			addImageToTensor(tlabel, data[index].second.imagePath[k], k+1, imageWidth,imageHeight);
	}

	return { tdata, tlabel };
}

void CustomDataset::addImageToTensor(at::Tensor &t, string imagePath, int id, int imageWidth, int imageHeight) {
	ofImage imgMask(imagePath);
	auto matMask = cv::Mat(imgMask.getHeight(), imgMask.getWidth(), CV_MAKETYPE(CV_8U, imgMask.getPixels().getNumChannels()), imgMask.getPixels().getData());//cv::imread(path);
	assert(!matMask.empty());

	cv::resize(matMask, matMask, cv::Size(imageWidth, imageHeight));
	cv::Mat tempImageMask;
	matMask.convertTo(tempImageMask, CV_32FC1, 1.f / 255);	
	auto maskTensor		= torch::from_blob(tempImageMask.ptr<float>(), { imageWidth, imageHeight }, torch::kFloat);
	auto maskTensorTemp = torch::cat({ maskTensor }).view({ 1, imageHeight,imageWidth });
	t = torch::cat({ t, maskTensorTemp }).view({ id, imageHeight, imageWidth });
}