#pragma once

#include <torch/arg.h>
#include <torch/nn/module.h>
#include <torch/optim/optimizer.h>
#include <torch/types.h>
#include <torch/optim/serialize.h>

#include <cstddef>
#include <utility>
#include <vector>

namespace torch {
namespace serialize {
class OutputArchive;
class InputArchive;
} // namespace serialize
} // namespace torch

namespace torch {
namespace optim {

struct  AdadeltaOptions {
  /* implicit */ AdadeltaOptions(double learning_rate);
  TORCH_ARG(double, learning_rate);
  TORCH_ARG(double, rho) = 0.95;
  TORCH_ARG(double, eps) = 1e-6;
  TORCH_ARG(double, weight_decay) = 0;
};

class  Adadelta : public Optimizer {
 public:
  template <typename ParameterContainer>
  explicit Adadelta(ParameterContainer&& parameters, const AdadeltaOptions& options)
      : Optimizer(std::forward<ParameterContainer>(parameters)),
        options(options) {}

  void step() override;

  void save(serialize::OutputArchive& archive) const override;
  void load(serialize::InputArchive& archive) override;

  AdadeltaOptions options;

	std::vector<int64_t> step_buffers;
	std::vector<Tensor> square_avg_buffers;
	std::vector<Tensor> acc_delta_buffers;

 private:
	 Adadelta() : options(0) {}

	 template <typename Self, typename Archive>
	 static void serialize(Self& self, Archive& archive) {
		 _TORCH_OPTIM_SERIALIZE(step_buffers);
		 _TORCH_OPTIM_SERIALIZE(square_avg_buffers);
		 _TORCH_OPTIM_SERIALIZE(acc_delta_buffers);
	 }
};
} // namespace optim
} // namespace torch
