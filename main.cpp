/*#include <opencv2/opencv.hpp>
#include <stdint.h>
#include <torch/torch.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>*/
#include <torch/torch.h>
#include "ofMain.h"
#include "ofApp.h"




int main() {
	torch::manual_seed(1); 

	ofGLWindowSettings settings;
	settings.setGLVersion(2, 1);
	settings.setSize(1280, 768);
	ofCreateWindow(settings);		// <-------- setup the GL context

									// this kicks off the running of my app
									// can be OF_WINDOW or OF_FULLSCREEN
									// pass in width and height too:
	ofRunApp(new ofApp());
		

	return 0;
}
