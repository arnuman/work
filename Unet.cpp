#include "Unet.h"


void initKernel(torch::nn::Conv2d& layer, int kernelSize = 9) {
	//torch::nn::init::normal_(layer->weight, 0.0, sqrtf(2.f / (0.5*layer->options.input_channels() * kernelSize)));
	if(kernelSize > 1) 
		torch::nn::init::kaiming_normal_(layer->weight,0,torch::nn::init::FanMode::FanIn,torch::nn::init::Nonlinearity::ReLU);
	else torch::nn::init::kaiming_normal_(layer->weight, 0, torch::nn::init::FanMode::FanIn, torch::nn::init::Nonlinearity::Sigmoid);
}
 
Unet::Unet(int n, int numClasses) :
	
	

	

	conv1(torch::nn::Conv2dOptions(3, 64 / n, 3).padding(1)),
	conv2(torch::nn::Conv2dOptions(64 / n, 64 / n, 3).padding(1)),
	conv3(torch::nn::Conv2dOptions(64 / n, 128 / n, 3).padding(1)),
	conv4(torch::nn::Conv2dOptions(128 / n, 128 / n, 3).padding(1)),
	conv5(torch::nn::Conv2dOptions(128 / n, 256 / n, 3).padding(1)),
	conv6(torch::nn::Conv2dOptions(256 / n, 256 / n, 3).padding(1)),
	conv7(torch::nn::Conv2dOptions(256 / n, 512 / n, 3).padding(1)),
	conv8(torch::nn::Conv2dOptions(512 / n, 512 / n, 3).padding(1)),

	convUp1(torch::nn::Conv2dOptions(512 / n, 256 / n, 3).padding(1)),
	/// ������������, �� ����� x2 - ����� ���� �� 2 �����:
	convUp2(torch::nn::Conv2dOptions(2 * 256 / n, 256 / n, 3).padding(1)),
	convUp3(torch::nn::Conv2dOptions(256 / n, 256 / n, 3).padding(1)),

	convUp4(torch::nn::Conv2dOptions(256 / n, 128 / n, 3).padding(1)),
	/// ������������, �� ����� x2 - ����� ���� �� 2 �����:
	convUp5(torch::nn::Conv2dOptions(2 * 128 / n, 128 / n, 3).padding(1)),
	convUp6(torch::nn::Conv2dOptions(128 / n, 128 / n, 3).padding(1)),

	convUp7(torch::nn::Conv2dOptions(128 / n, 64 / n, 3).padding(1)),
	/// ������������, �� ����� x2 - ����� ���� �� 2 �����:
	convUp8(torch::nn::Conv2dOptions(2 * 64 / n, 64 / n, 3).padding(1)),

	convUp9(torch::nn::Conv2dOptions(64 / n, 64 / n, 3).padding(1)),
	  
	/*convUp10(torch::nn::Conv2dOptions(64 / n, 64 / n, 3).padding(1)),
	convUp11(torch::nn::Conv2dOptions(64 / n, numClasses, 1))	*/

	/*convUp10(torch::nn::Conv2dOptions(64 / n, 2 * numClasses, 3).padding(1)),
	convUp11(torch::nn::Conv2dOptions(2 * numClasses, numClasses, 1))*/

	convUp10(torch::nn::Conv2dOptions(64 / n, 32, 3).padding(1)),
	convUp11(torch::nn::Conv2dOptions(32, numClasses, 1))

	/*convUp10(torch::nn::Conv2dOptions(64 / n, 2 , 3).padding(1)),
	convUp11(torch::nn::Conv2dOptions(2 , numClasses, 1))*/
	 
{ 
	register_module("conv1", conv1);
	register_module("conv2", conv2);
	register_module("conv3", conv3);
	register_module("conv4", conv4);
	register_module("conv5", conv5);
	register_module("conv6", conv6);
	register_module("conv7", conv7);
	register_module("conv8", conv8);

	register_module("convup1", convUp1);
	register_module("convup2", convUp2);
	register_module("convup3", convUp3);
	register_module("convup4", convUp4);
	register_module("convup5", convUp5);
	register_module("convup6", convUp6);
	register_module("convup7", convUp7);
	register_module("convup8", convUp8);
	register_module("convup9", convUp9);
	register_module("convup10", convUp10);
	register_module("convup11", convUp11);
	
	initKernel(conv1);
	initKernel(conv2);
	initKernel(conv3);
	initKernel(conv4);
	initKernel(conv5);
	initKernel(conv6);
	initKernel(conv7);
	initKernel(conv8);
	initKernel(convUp1);
	initKernel(convUp2);
	initKernel(convUp3);
	initKernel(convUp4);
	initKernel(convUp5);
	initKernel(convUp6);
	initKernel(convUp7);
	initKernel(convUp8);
	initKernel(convUp9);
	initKernel(convUp10);
	initKernel(convUp11, 1);

}

torch::Tensor Unet::forward(torch::Tensor x) {

	x = torch::relu(conv1->forward(x));
	auto conv2Relu = torch::relu(conv2->forward(x));

	x = torch::max_pool2d(conv2Relu, 2);

	x = torch::relu(conv3->forward(x));
	auto conv4Relu = torch::relu(conv4->forward(x));
	x = torch::max_pool2d(conv4Relu, 2);



	x = torch::relu(conv5->forward(x));
	auto conv6Relu = torch::relu(conv6->forward(x));
	x = torch::max_pool2d(conv6Relu, 2);

	x = torch::relu(conv7->forward(x));
	x = torch::relu(conv8->forward(x));
	
	x = torch::dropout(x, 0.5, is_training());


	auto sizes = x.sizes();
	auto w = sizes[2] * 2;
	auto h = sizes[3] * 2;

	x = torch::upsample_bilinear2d(x, { w, h }, true);
	x = torch::relu(convUp1->forward(x));

	//x = torch::relu(convUp2->forward(x));		
	x = torch::relu(convUp2->forward(torch::cat({ x, conv6Relu }, 1)));
	x = torch::relu(convUp3->forward(x));

	x = torch::dropout(x, 0.5, is_training());

	sizes = x.sizes();
	w = sizes[2] * 2; 
	h = sizes[3] * 2;

	x = torch::upsample_bilinear2d(x, { w, h }, true);
	x = torch::relu(convUp4->forward(x));
	x = torch::relu(convUp5->forward(torch::cat({ x, conv4Relu }, 1)));
	//x = torch::relu(convUp5->forward(x));
	x = torch::relu(convUp6->forward(x));
	 
	x = torch::dropout(x, 0.5, is_training());

	sizes = x.sizes();
	w = sizes[2] * 2;
	h = sizes[3] * 2;

	x = torch::upsample_bilinear2d(x, { w, h }, true);
	x = torch::relu(convUp7->forward(x));


	x = torch::relu(convUp8->forward(torch::cat({ x, conv2Relu }, 1)));
	//x = torch::relu(convUp8->forward(x));
	x = torch::relu(convUp9->forward(x));

	x = torch::dropout(x, 0.5, is_training());

	x = torch::relu(convUp10->forward(x));
	x = convUp11->forward(x);


	return torch::sigmoid(x);
}

