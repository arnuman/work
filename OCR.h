#pragma once
#include <allheaders.h> // leptonica main header for image io
#include <tesseract/baseapi.h> // tesseract main header
#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "PageNode.h"
#include "ofxOpenCv.h"

//��������� �������� 
#define NEURO_INPUT_SIZE 256

struct Label
{
	Label(string tag, float  value) {
		this->tag = tag;
		this->value = value;
	}
	string tag;
	float  value;
};

class OCR {
	public:

		~OCR();

		void setup();
		int getPageOrientation(const unsigned char* imagedata, int width, int height, int bytes_per_pixel, int bytes_per_line);
		float getSkew(const unsigned char* imagedata, int width, int height, int bytes_per_pixel, int bytes_per_line);
		void recognizePage(const unsigned char* imagedata, int width, int height,int bytes_per_pixel, int bytes_per_line);
		void reRecognizeEmptyWords(ofPixels& pix);
		void reRecognize(ofPixels& pix);


		string recognizeArea(const unsigned char* imagedata, int width, int height, int bytes_per_pixel, int bytes_per_line);
		bool isEmptyString(string& str);
		PageNode* getPageData();

		void saveToXml(bool isSavelLabel = true);
		void loadFromXml();

		void drawPageAsBoxes(int x = 0, int y = 0);
		void drawLabels();

		void updateNodeRects();
		void updatePageAsBoxesFbo(bool save = false);

		void keyPressed(int key);

		string getLabelsXmlPath();
		string getResXmlPath();
		string getPageFolderPath();
		string getDocumentFolderPath();
		string getPageFolderPathByPageID(unsigned int id);
		ofTrueTypeFont& getFont();
		bool   existPage();

		void initLabels();


		/*string	currentPdfFileName;
		int		pageId;*/

		string	pdfFileName;
		string	pdfFolderName;
		int		pageId;

	private:

		bool checkInterception(ofRectangle& rect, PageNode * page, bool& flag);
		void mergeBlobs(vector<ofRectangle>& vector);
		void addWordToXML(PageNode* page, string& arrayWords, ofRectangle& rect);
		
		ofTrueTypeFont font;

		ofRectangle getBoundingBox(ResultIterator* it, PageIteratorLevel level);

		TessBaseAPI tess;
		string		findText;
		PageNode*	rootNode;
		PageNode*	currentNode;




		vector<NodeRect> rects;
		//ofFbo fboRects;
		//ofFbo fboRectsSmall;
		
		//��� ��� � ������ ����� �� 4 ���� �� �������� 4 ����� ����
		vector<ofPixels*> fboWords;
		vector<ofPixels*> fboWordsSmall;
		
		//������ ������� ����� ��� �������� 
		int neuroInputSize;

		//������ ������� ��������� �� ����� settings.xml, � �������� �� ���������
		vector<Label>  labelsFromSettingsXml;

		//������ ������� ������� ���� ������������ � ������ ���������
		vector<Label>  labelsFromPage;
		//��������� ������
		vector<Label>  labelsResult;
};
