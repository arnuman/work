#pragma once 

#include <torch/torch.h>
#include "ofMain.h"
#include <codecvt>

struct Options {	
	int image_width = 256;
	int image_height = 384;
	string pathToXML = "";
	string pathToLabel = "";
	size_t train_batch_size = 2;
	size_t test_batch_size = 1;
	size_t iterations = 200;
	size_t log_interval = 2;
	//size_t train_to_test_ratio = 0.7;
	/// ����� �������-�������� ������� ���� ������ ������������
	/// ���������� ����� ������� ��������� ���� ����:
	size_t num_classes = 1;
	// path must end in delimiter
	std::string datasetPath = "deffect_dataset/";
	std::string infoFilePath = "dataset_x.txt";
	torch::DeviceType device = torch::kCUDA;
	float learningRate;
	vector<string> classesNames;
};

enum ImageTransform {
	ImageTransformNone = 0,
	ImageTransformFlipV,
	ImageTransformFlipH,
	ImageTransformFlipVH
};

struct DataSample{
	DataSample():tranform(ImageTransformNone){}
	//string imagePath;
	/// ��� ���������� ���������� �������/����� � ������ ������ ������ ������:
	std::vector<string> imagePath;
	std::string			rootDir;
	ImageTransform tranform;
};


using Data = std::vector<std::pair<DataSample, DataSample>>;

class CustomDataset : public torch::data::datasets::Dataset<CustomDataset> {
	using Example = torch::data::Example<>;

	Data data;

public:
	CustomDataset(const Data& data, const Options& options) : data(data), options(options){}
	Example get(size_t index);
	
	void addImageToTensor(at::Tensor &t, string imagePath, int id, int imageWidth, int imageHeight);

	torch::optional<size_t> size() const {
		return data.size();
	}

	Options options;
};

inline bool isNonEmptyFolder(string folder) {
	//cout << folder  << "/page.png" << endl;
	bool isPageExist = ofFile::doesFileExist(folder + "/page.png", false);
	bool isWords0Exist = ofFile::doesFileExist(folder + "/Words_0.png", false);
	bool isWords1Exist = ofFile::doesFileExist(folder + "/Words_1.png", false);
	bool isLabelExist = ofFile::doesFileExist(folder + "/label.png", false);
	bool isLabelExist2 = ofFile::doesFileExist(folder + "/label_2.png", false);
	bool isLabelExist3 = ofFile::doesFileExist(folder + "/label_3.png", false);
	return isPageExist && isWords0Exist && isWords1Exist && isLabelExist && isLabelExist2 && isLabelExist3;
}


inline void getListFiles(std::filesystem::path rootDir, Data& dataAI) {
	ofDirectory data = ofDirectory(rootDir);
	data.listDir();
	vector<ofFile> filesTemp = data.getFiles();
	for (size_t i = 0; i < filesTemp.size(); i++) {
		//	cout << "isNonEmptyFolder(filesTemp[i].getAbsolutePath(): " << isNonEmptyFolder(filesTemp[i].getAbsolutePath()) << endl;
		if (filesTemp[i].getBaseName().find("segmentation_label") != string::npos && isNonEmptyFolder(filesTemp[i].getAbsolutePath())) {
			DataSample image;
			image.rootDir = filesTemp[i].getAbsolutePath();
			image.imagePath.push_back(filesTemp[i].getAbsolutePath() + "/page.png");
			image.imagePath.push_back(filesTemp[i].getAbsolutePath() + "/Words_0.png");
			image.imagePath.push_back(filesTemp[i].getAbsolutePath() + "/Words_1.png");
			image.tranform = (ImageTransform)0;
			DataSample label;
			label.imagePath.push_back(filesTemp[i].getAbsolutePath() + "/label.png");
			label.imagePath.push_back(filesTemp[i].getAbsolutePath() + "/label_2.png");
			label.imagePath.push_back(filesTemp[i].getAbsolutePath() + "/label_3.png");
			label.tranform = (ImageTransform)0;
			dataAI.push_back(std::make_pair(image, label));
		} else if (filesTemp[i].isDirectory()) getListFiles(filesTemp[i], dataAI);
	}
}


inline std::pair<Data, Data> readInfoTextSegmentation(Options& options) {
	Data train, test;
	Data allData;
	cout << "datasetpath: " << options.datasetPath << endl;
	getListFiles(options.datasetPath, allData);
	std::random_shuffle(allData.begin(), allData.end());
	
	float train_to_test_ratio = 0.8;

	for (size_t i = 0; i < allData.size() * train_to_test_ratio; i++)
		train.push_back(allData[i]);

	ofstream testFolderList(ofToDataPath("testFolderList.txt"));
	for (size_t i = allData.size() * train_to_test_ratio; i < allData.size(); i++) {
		test.push_back(allData[i]);
		testFolderList << allData[i].first.rootDir << endl;
	}
	testFolderList.close();

	cout << "num train samples: " << train.size() << endl;
	cout << "num test samples: " << test.size() << endl;

	return std::make_pair(train, test);
}

inline std::pair<Data, Data> readInfo(Options& options) {
	Data train, test;

	std::ifstream stream(options.infoFilePath);
	//assert(stream.is_open());

	string label;
	std::string path, type;

	ofDirectory trainDir;
	trainDir.listDir(options.datasetPath + "/train/image");
	for (size_t i = 0; i < trainDir.numFiles(); i++)
	{
		if (trainDir.getPath(i).find(".db") == string::npos) {
			string pathImage = trainDir.getPath(i);
			string labelPath = options.datasetPath +  "/train/label/" + trainDir.getName(i);

			/*cout << "labelPath = " << labelPath << endl;
			cout << "pathImage = " << pathImage << endl;*/
			for (size_t j = 0; j < 4; j++){
				DataSample image;
				image.imagePath.push_back(pathImage);
				image.tranform = (ImageTransform)j;			
				DataSample label;

				if (options.num_classes == 1)
					label.imagePath.push_back(labelPath);

				label.tranform = (ImageTransform)j;

				/// ���������� ���������������� ������������� - ��������� ����������� � ��������� �����:
				if (options.num_classes > 1) {
					for (size_t k = 0; k < options.num_classes; k++){			
						string multiLabelPath = options.datasetPath + "/train/label_" + options.classesNames[k] + "/" + trainDir.getName(i);
						label.imagePath.push_back(multiLabelPath);
					}
				}
				train.push_back(std::make_pair(image, label));
			}
		}
	}

	ofDirectory testDir;
	testDir.listDir(options.datasetPath + "/test/image");
	testDir.sort();
	for (size_t i = 0; i < testDir.numFiles(); i++){
		if (testDir.getPath(i).find(".db") == string::npos) {

			string pathImage = testDir.getPath(i);
			string labelPath = options.datasetPath + "/test/label/" + testDir.getName(i);
			for (size_t j = 0; j < 4; j++) {
				DataSample image;
				image.imagePath.push_back(pathImage);
				image.tranform = (ImageTransform)j;

				DataSample label;
				if (options.num_classes == 1)
					label.imagePath.push_back(labelPath);
				label.tranform = (ImageTransform)j;

				if (options.num_classes > 1) {
					for (size_t k = 0; k < options.num_classes; k++) {
						string multiLabelPath = options.datasetPath + "/test/label_" + options.classesNames[k] + "/" + testDir.getName(i);
						label.imagePath.push_back(multiLabelPath);
					}
				}


				test.push_back(std::make_pair(image, label));
			}
			//test.push_back(std::make_pair(pathImage, pathImage));
		}
	}

	cout << "load dataset complete!" << endl;

	std::random_shuffle(train.begin(), train.end());
	//std::random_shuffle(test.begin(), test.end());
	return std::make_pair(train, test);
}
