#include "ofApp.h"
#include <opencv2/opencv.hpp>

//--------------------------------------------------------------
void ofApp::setup() {

	setupUnetFromXML();
	setupSemnentationFromXML();

	ofTrueTypeFontSettings settings("verdana.ttf", 12);
	settings.antialiased = true;
	settings.addRange(ofUnicode::Cyrillic);
	settings.addRange(ofUnicode::Space);
	settings.addRange(ofUnicode::Latin);
	settings.addRange(ofUnicode::LetterLikeSymbols);
	font.load(settings);

	getListFiles(options.datasetPath);

	isDrawResultSegmentation = true;
}

void ofApp::setupSemnentationFromXML() {
	ofxXmlSettings segmentationXML("autoSegmentation_settings.xml");
	segmentationXML.pushTag("autoSegmentation_config");

	segmentationXML.pushTag("imageSize");
	options.image_width = segmentationXML.getValue("width", 256);
	options.image_height = segmentationXML.getValue("height", 384);
	segmentationXML.popTag();

	segmentationXML.pushTag("attributeClasses");
	num_classesSegmentation = max(1, segmentationXML.getNumTags("name"));
	for (size_t i = 0; i < num_classesSegmentation; i++)
		classesNamesSegmentation.push_back(segmentationXML.getValue("name", "", i));
	segmentationXML.popTag();

	binarization = segmentationXML.getValue("binarization", 100);
	segmentationXML.pushTag("threshold");
	thresholdTextline = segmentationXML.getValue("textline", 0.6);
	thresholdWord = segmentationXML.getValue("word", 0.4);
	segmentationXML.popTag();
}

void ofApp::setupUnetFromXML() {
	ofxXmlSettings unetXML("unet_settings.xml");
	unetXML.pushTag("unet_config");

	if (unetXML.getValue("typeDevice", "CPU") == "CUDA") options.device = torch::kCUDA;
	else options.device = torch::kCPU;

	std::cout << "Running on: " << (options.device == torch::kCUDA ? "CUDA" : "CPU") << std::endl;

	options.iterations = unetXML.getValue("numEpoch", 1);
	options.train_batch_size = unetXML.getValue("trainBatchSize", 1);
	minBestLoss = unetXML.getValue("bestLossMin", 1.0f);
	unetScaleDevider = unetXML.getValue("unetSizeDevider", 1);
	options.learningRate = unetXML.getValue("lr", 1e-4);

	unetXML.pushTag("recognizeClasses");
	options.num_classes = max(1, unetXML.getNumTags("name"));
	for (size_t i = 0; i < options.num_classes; i++)
		options.classesNames.push_back(unetXML.getValue("name", "", i));
	unetXML.popTag();

	options.datasetPath = unetXML.getValue("datasetPath", "deffect_dataset");
	loadParamsPath = unetXML.getValue("paramsPath", "");

	if (!loadParamsPath.empty()) loadModel(ofToDataPath("params/" + loadParamsPath));

	segmentationMaps.resize(options.num_classes);
	labelImages.resize(options.num_classes);
}
/*
bool ofApp::isFindFileFolder(string folder, string file) {
	return ofFile::doesFileExist(folder + "/" + file, false);
}*/

void ofApp::loadImages(string path) {
	ofImage img;
	if (!img.loadImage(path + "/page.png"))
		return;
	smallPage = img;
	if (!img.loadImage(path + "/Words_0.png"))
		return;
	words0 = img;
	if (!img.loadImage(path + "/Words_1.png"))
		return;
	words1 = img;
	//predictImage();
}

void ofApp::getListFiles(std::filesystem::path rootDir) {
	ofDirectory data = ofDirectory(rootDir);
	data.listDir();

	vector<ofFile> filesTemp = data.getFiles();
	for (size_t i = 0; i < filesTemp.size(); i++) {
		if (filesTemp[i].getBaseName().find("segmentation_label") != string::npos)
		{
			options.pathToLabel = filesTemp[i].getAbsolutePath();
			options.pathToXML = filesTemp[i].getEnclosingDirectory();
			if (!isNonEmptyFolder(filesTemp[i].getAbsolutePath())) {
				
				ofDirectory dataEnclosing = ofDirectory(filesTemp[i].getEnclosingDirectory());				
				dataEnclosing.listDir();
				vector<ofFile> filesTempEnclosing = dataEnclosing.getFiles();
				string res = "";
				for (size_t j = 0; j < filesTempEnclosing.size(); j++) {
					res = filesTempEnclosing[j].getBaseName() + "." + filesTempEnclosing[j].getExtension();
					if (res == "res.xml") {						
						loadFromXml(filesTempEnclosing[j].getEnclosingDirectory());
						if (smallPage.size() != NULL)
							predictImage(); 
						return;
					} 
				}

				if (res != "res.xml")
					cout << "Not found res.xml!" << endl;
			}
			else {
				loadImages(filesTemp[i].getAbsolutePath());
				loadFromXml(filesTemp[i].getEnclosingDirectory());
				predictImage();
				return;
			}

		}		
		else if (filesTemp[i].isDirectory()) getListFiles(filesTemp[i]);
	}
}

/*
void ofApp::getListFiles1(std::filesystem::path rootDir) {
	ofDirectory data = ofDirectory(rootDir);
	data.listDir();

	vector<ofFile> filesTemp = data.getFiles();
	if (filesTemp.size() == 0)
		cout << "Not found res.xml!" << endl;
	for (size_t i = 0; i < filesTemp.size(); i++) {

		if (isFindFileFolder(filesTemp[i].getAbsolutePath(), "res.xml"))
		{
			if (!(isFindFileFolder(filesTemp[i].getAbsolutePath(), "Words_0.png")) &&
				!(isFindFileFolder(filesTemp[i].getAbsolutePath(), "Words_1.png")))
				loadFromXml(filesTemp[i].getAbsolutePath());
			if ((words0.size() == NULL) || (words1.size() == NULL))	{
				labelImages[0].loadImage(filesTemp[i].getAbsolutePath() + "/Words_0.png");
				labelImages[1].loadImage(filesTemp[i].getAbsolutePath() + "/Words_1.png");
				words0 = labelImages[0];
				words1 = labelImages[1];
			}
			else {
				labelImages[0].setFromPixels(words0);
				labelImages[1].setFromPixels(words1);
			}
			options.pathToXML = filesTemp[i].getAbsolutePath();
			if ((labelImages[0].getPixels().size() != NULL) && (labelImages[1].getPixels().size() != NULL))
				predictImage();

			words0.clear();
			words1.clear();
		}
		else if (filesTemp[i].isDirectory()) getListFiles1(filesTemp[i]);
	}
}
*/
//--------------------------------------------------------------
void ofApp::update() {

}

//--------------------------------------------------------------
void ofApp::draw() {
	ofPushMatrix();

	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2);
	ofScale(scaleImage, scaleImage);
	ofTranslate(-ofGetWidth() / 2, -ofGetHeight() / 2);
	ofTranslate(canvasPos);

	if (bigPage.isAllocated())		bigPage.draw(0, 0);
	contourFinderRed.draw();
	contourFinderGreen.draw();
	contourFinderBlue.draw();

	/*if (drawVector.size() > 0)
	{
		for (size_t i = 0; i < drawVector.size(); i++) {
			ofSetColor(0, 0, 255);
			//ofNoFill();
			ofRect(drawVector[i]);
			ofFill();
		}
	}*/
	if (drawVectorRed.size() > 0) {
		for (size_t i = 0; i < drawVectorRed.size(); i++) {
			ofSetColor(255, 0, 0);
			//ofNoFill();
			ofRect(drawVectorRed[i]);
			ofFill();
		}
	}

	if (drawVectorGreen.size() > 0)	{
		for (size_t i = 0; i < drawVectorGreen.size(); i++) {
			ofSetColor(0, 255, 0);
			//ofNoFill();
			ofRect(drawVectorGreen[i]);
			ofFill();
		}
	}
	if (drawVectorBlue.size() > 0) {
		for (size_t i = 0; i < drawVectorBlue.size(); i++) {
			ofSetColor(0, 0, 255);
			//ofNoFill();
			ofRect(drawVectorBlue[i]);
			ofFill();
		}
	}

	for (size_t i = 0; i < labelImages.size(); i++) {
		if (labelImages[i].isAllocated()) {
			ofDrawBitmapString("label_" + ofToString(i), bigPage.getWidth() + 50, (10 + labelImages[i].getHeight())*i);
			labelImages[i].draw(bigPage.getWidth() + 50, (10 + labelImages[i].getHeight()) * i);
		}
	}

	for (size_t i = 0; i < segmentationMaps.size(); i++)
		if (segmentationMaps[i].isAllocated())	segmentationMaps[i].draw(bigPage.getWidth() + 70 + labelImages[i].getWidth(), (10 + segmentationMaps[i].getHeight()) * i);


	ofSetColor(255, 128);

	ofEnableBlendMode(OF_BLENDMODE_SCREEN);

	for (size_t i = 0; i < segmentationMaps.size() && isDrawResultSegmentation; i++) {
		if (segmentationMaps[i].isAllocated()) {
			if (i == 0) ofSetColor(ofColor::red);
			if (i == 1) ofSetColor(ofColor::green);
			if (i == 2) ofSetColor(ofColor::blue);
			segmentationMaps[i].draw(0, 0, bigPage.getWidth(), bigPage.getHeight());
			ofSetColor(ofColor::white);
		}
	}
	
	ofSetColor(255);

	ofEnableBlendMode(OF_BLENDMODE_ALPHA);

	ofPopMatrix();
}


void ofApp::mouseScrolled(ofMouseEventArgs& mouse) {
	scaleImage += mouse.scrollY*0.1;
	if (scaleImage < 0) scaleImage = abs(scaleImage);
}

void ofApp::loadFromXml(string path) {
	delete rootNode;

	ofxXmlSettings xml;
	ofxXmlSettings xmlNode;
	cout << path << endl;
	xmlNode.loadFile(path + "/res.xml");
	currentNode = rootNode = new PageNode(NULL, &font);

	//������� ��������� ���
	rootNode->createFromXml(xmlNode, 0);
	
	updatePageAsBoxesFbo(path);
};

void ofApp::updateNodeRects() {
	rects.clear();
	rootNode->addToNodeRectsVector(rects);
}

void ofApp::updatePageAsBoxesFbo(string path) {
	if (rootNode == NULL) return;

	vector<ofPixels*> fboWordsSmall;
	//��������� ��������������
	updateNodeRects();

	//�������� � smallMapSize
	float scaleWidth = (float)options.image_width / rootNode->body.width;
	float scaleHeight = (float)options.image_height / rootNode->body.height;

	if (fboWordsSmall.size()) {
		for (size_t i = 0; i < fboWordsSmall.size(); i++) delete fboWordsSmall[i];
		fboWordsSmall.clear();
	}

	//������� �����
	for (size_t i = 0; i < 4; i++) {
		fboWordsSmall.push_back(new ofPixels());
		fboWordsSmall.back()->allocate(options.image_width, options.image_height, ofImageType::OF_IMAGE_GRAYSCALE);
		fboWordsSmall.back()->setColor(0);
	}


	UINT64 strt = ofGetElapsedTimeMillis();
	//�������� �����
	for (size_t i = 0; i < rects.size(); i++) {
		NodeRect & rect = rects[i];
		if (rect.type == PageIteratorLevel::RIL_WORD &&  rootNode->body.height*0.5 > rect.rect.height) {

			//��� ������ ������� �������� �������
			float width = rect.rect.width / (float)(rect.symbols.size());

			for (size_t j = 0; j < rect.symbols.size(); j++) {
				for (size_t k = 0; k < rect.symbols[j].size(); k++)	{
					//����������� ������
					int startY = floor(rect.rect.y * scaleHeight);
					int endY = ceil(startY + rect.rect.height* scaleHeight);
					for (int y = startY; y < endY; y++)		{
						int startX = floor(rect.rect.x * scaleWidth + j * width * scaleWidth);
						int endX = ceil(startX + width * scaleWidth);
						for (int x = startX; x < endX; x++)
							fboWordsSmall[k]->getPixels()[x + y * fboWordsSmall[k]->getWidth()] = rect.symbols[j][k];						
					}
				}
			}
		}
	}

	UINT64 end = ofGetElapsedTimeMillis();
	
	ofImage img;
	if (!img.loadImage(path + "/page.png"))
		return;
	img.resize(options.image_width, options.image_height);
	smallPage = img;

	words0 = *fboWordsSmall[0];
	words1 = *fboWordsSmall[1];

	fboWordsSmall.clear();
}

template <typename DataLoader>
void ofApp::train(Unet& network, DataLoader& loader, torch::optim::Optimizer& optimizer, size_t epoch, size_t data_size, Options& options) {

	size_t index = 0;
	network.train();

	float Loss = 0;
	   
	for (auto& batch : loader) {
		auto data = batch.data.to(options.device);
		auto targets = batch.target.to(options.device);

		optimizer.zero_grad();
		auto output = network.forward(data);

		auto loss = torch::binary_cross_entropy(output, targets);
		//auto loss = torch::multi_margin_loss(output, targets);
		assert(!std::isnan(loss.template item<float>()));

		loss.backward();
		optimizer.step();

		Loss += loss.template item<float>();

		if (index++ % options.log_interval == 0) {
			auto end = std::min(data_size, (index + 1) * options.train_batch_size);

			std::cout << "Train Epoch: " << epoch << " " << end << "/" << data_size
				<< "\tLoss: " << Loss / end << std::endl;
		}
	}

	lossGlobal = Loss / data_size;
	cout << "loss avg epoch = " << lossGlobal << endl;
}

float ofApp::calcAccuracy(at::Tensor& outTensor, at::Tensor& targetTensor) {

	int numPositiveOverlap = numPositiveInTensor(targetTensor, outTensor);
	int numPositiveTarget = numPositiveInTensor(targetTensor);

	/*cout << "numPositiveOverlap = " << numPositiveOverlap << endl;
	cout << "numPositiveTarget = "  << numPositiveTarget << endl;*/

	return ofMap(numPositiveOverlap, 0, numPositiveTarget, 0.f, 100.f, true);
}

int ofApp::numPositiveInTensor(at::Tensor& t, at::Tensor& o) {
	auto sizeTensor = t.sizes();
	int numPositive = 0;
	int id = 0;
	float * data = t.data<float>();
	float * data2 = o.data<float>();
	for (size_t k = 0; k < sizeTensor[0]; k++) {
		for (size_t z = 0; z < sizeTensor[1]; z++) {
			for (size_t x = 0; x < sizeTensor[2]; x++) {
				for (size_t y = 0; y < sizeTensor[3]; y++) {
					if (data[id] > 0 && data2[id] > 0) numPositive++;
					id++;
				}
			}
		}
	}
	return numPositive;
}

int ofApp::numPositiveInTensor(at::Tensor& t) {
	auto sizeTensor = t.sizes();
	int numPositive = 0;
	int id = 0;
	float * data = t.data<float>();
	for (size_t k = 0; k < sizeTensor[0]; k++) {
		for (size_t z = 0; z < sizeTensor[1]; z++) {
			for (size_t x = 0; x < sizeTensor[2]; x++) {
				for (size_t y = 0; y < sizeTensor[3]; y++) {
					if (data[id] > 0) numPositive++;
					id++;
				}
			}
		}
	}
	return numPositive;
}

template <typename DataLoader>
void ofApp::test(Unet& network, DataLoader& loader, size_t data_size, int numEpoch, Options& options) {

	size_t index = 0;
	network.eval();
	torch::NoGradGuard no_grad;
	float Loss = 0;
	float accuracy = 0;

	int numID = 0;


	for (const auto& batch : loader) {
		auto data = batch.data.to(options.device);
		auto targets = batch.target.to(options.device);

		auto output = network.forward(data);
		auto outputCpu = output.cpu();

		auto sizeOut = outputCpu.sizes();

		int batchSize = sizeOut[0];
		int numOutMaps = sizeOut[1];
		int widthMap = sizeOut[2];
		int heightMap = sizeOut[3];
		for (size_t batchID = 0; batchID < batchSize; batchID++) {
			for (size_t mapID = 0; mapID < numOutMaps; mapID++) {
				float * arrayOut = outputCpu.data<float>();
				ofFloatPixels pix;
				//predictPixels.setFromPixels(arrayOut, heightMap, widthMap, 1);
				pix.setFromPixels(arrayOut + (batchID * numOutMaps * widthMap * heightMap) + (mapID*(widthMap * heightMap)), heightMap, widthMap, 1);

				if (numOutMaps == 1)	ofSaveImage(pix, "out/out_test_epoch_" + ofToString(numEpoch) + "_image_" + ofToString(numID) + ".jpg");
				else ofSaveImage(pix, "out/" + options.classesNames[mapID] + "/out_test_epoch_" + ofToString(numEpoch) + "_image_" + ofToString(numID) + ".jpg");
			}

		}

		auto loss = torch::binary_cross_entropy(output, targets);

		assert(!std::isnan(loss.template item<float>()));
		Loss += loss.template item<float>();
		numID++;
	}

	if (index++ % options.log_interval == 0) {
		testLossGlobal = Loss / data_size;
		testAccuracyGlobal = accuracy / data_size;
		std::cout << "Test Loss: " << testLossGlobal
			<< std::endl;
	}
}

void ofApp::loadModel(string fileName) {

	loadParamsPath = fileName;

	cout << "start load model from file :" << fileName << endl;

	network.reset();
	network = std::make_shared<Unet>(Unet());//Unet network;

	torch::load(network, fileName);
	network->to(options.device);

	cout << "loading unet model from file" << endl;
}

void ofApp::addImageToTensor(at::Tensor &t, ofPixels &inputImagePixels, int id, int imageWidth, int imageHeight) {
	ofImage imgMask = inputImagePixels;

	auto matMask = cv::Mat(imgMask.getHeight(), imgMask.getWidth(), CV_MAKETYPE(CV_8U, imgMask.getPixels().getNumChannels()), imgMask.getPixels().getData());//cv::imread(path);
	assert(!matMask.empty());

	cv::resize(matMask, matMask, cv::Size(imageWidth, imageHeight));
	cv::Mat tempImageMask;
	matMask.convertTo(tempImageMask, CV_32FC1, 1.f / 255);
	auto maskTensor = torch::from_blob(tempImageMask.ptr<float>(), { imageWidth, imageHeight }, torch::kFloat);
	auto maskTensorTemp = torch::cat({ maskTensor }).view({ 1, imageHeight,imageWidth });
	if (id > 1) t = torch::cat({ t, maskTensorTemp }).view({ id, imageHeight, imageWidth });
	else t = maskTensorTemp;
}

void ofApp::predictImage() {
	if (network == NULL) return;

	vector<ofPixels> inputImagePixels;
	ai_out.clear();

	if (labelImages[0].load(options.pathToLabel + "/label.png") && labelImages[1].load(options.pathToLabel + "/label_2.png") && labelImages[2].load(options.pathToLabel + "/label_3.png")) {
		
		ofFile f(options.pathToXML);
		bigPage.loadImage(f.getAbsolutePath() + "/page.png");

		//ofImage smallPage(f.getEnclosingDirectory() + "/page.png");
		//smallPage.resize(options.image_width, options.image_height);
		//ofPixels pix;
		//pix = smallPage;

		inputImagePixels.push_back(smallPage);
		inputImagePixels.push_back(words0);
		inputImagePixels.push_back(words1);

		at::Tensor inputTensor;
		for (size_t i = 0; i < inputImagePixels.size(); i++)
			addImageToTensor(inputTensor, inputImagePixels[i], 1 + i, labelImages[0].getWidth(), labelImages[0].getHeight());

		inputTensor = inputTensor.view({ 1, (int)inputImagePixels.size(), (int)labelImages[0].getHeight(), (int)labelImages[0].getWidth() });

		network->eval();
		torch::NoGradGuard no_grad;
		auto data = inputTensor.to(options.device);
		auto output = network->forward(data);
		auto outputCpu = output.cpu();
		auto sizeOut = outputCpu.sizes();
		int batchSize = sizeOut[0];
		int numOutMaps = sizeOut[1];
		int widthMap = sizeOut[2];
		int heightMap = sizeOut[3];

		for (size_t batchID = 0; batchID < batchSize; batchID++) {
			for (size_t mapID = 0; mapID < numOutMaps; mapID++) {
				float * arrayOut = outputCpu.data<float>();
				ofFloatPixels pix;
				pix.setFromPixels(arrayOut + (batchID * numOutMaps * widthMap * heightMap) + (mapID*(widthMap * heightMap)), heightMap, widthMap, 1);
				segmentationMaps[mapID].setFromPixels(pix);
			}
		}
		for (size_t i = 0; i < segmentationMaps.size(); i++)
			ai_out.push_back(segmentationMaps[i].getPixelsRef());
		cout << "start " << endl;
		if (rootNode == NULL)
			cout << "end" << endl;
		computeBlackLines(words0, thresholdWord);
	}
}

bool ofApp::checkInterception(ofRectangle &rect, PageNode* page, bool &flag, float& threshold) {
	ofRectangle  rectInterception;
	for (size_t i = 0; i < page->children.size() && page->type != RIL_WORD; i++) {
		if (page->children[i]->getFirstWord() == "")
			continue;

		if (page->type == RIL_TEXTLINE) {
			rectInterception = page->body.getIntersection(rect);

			float square = page->body.getArea();
			float squareRectIntersept = rectInterception.getArea();

			if (square == 0 || squareRectIntersept == 0)
				continue;
			float squareIntersept = squareRectIntersept / square;
			if ((squareIntersept < threshold) && (squareIntersept > 0.2)) {
				for (size_t j = 0; j < page->children.size(); j++)	{
					rectInterception = page->children[j]->body.getIntersection(rect);

					if ((fabs(page->body.getCenter().y - rect.getCenter().y) >= 10))
						continue;
					if (!rectInterception.isZero() ||
						(fabs(rect.getBottomRight().x - page->children[j]->body.getBottomLeft().x) < 40) ||
						(fabs(rect.getBottomLeft().x - page->children[j]->body.getBottomRight().x) < 40))
					{
						rect.growToInclude(page->children[j]->body);
						flag = true;
					}
				}
			}
			if (!flag && squareIntersept > threshold) {
				if ((fabs(page->body.getCenter().y - rect.getCenter().y) <= 15) ||
					((page->body.y <= rect.getCenter().y) &&
					(page->body.y + page->body.getHeight() >= rect.getCenter().y)))
				{
					if (!rectInterception.isZero())	{
						rect = page->body;
						flag = true;
					}
				}
			}
		}
		else checkInterception(rect, page->children[i], flag, threshold);

		if (flag)
			return false;
	}
	if (!flag)
		return true;
}

void ofApp::computeBlackLines(ofPixels & pix, float threshold) {
	if (rootNode == NULL)
		return;

	vector<ofRectangle> vectorRect;
	vector<ofRectangle> vectorAIOut;
	vector<string>      vectorTypeWord;
	vector<string>      vectorTypeWordAIOut;
	cout << "start" << endl;
	for (size_t i = 0; i < ai_out.size(); i++) {
		bool flag = false;
		ofxCvGrayscaleImage grayImage;
		//ofxCvContourFinder contourFinder;
		ofImage img;
		ofPixels pixels;
		vector<ofRectangle> interceptionRect;
		string attribute;
		ofRectangle rect;

		cout << i << endl;
		attribute = classesNamesSegmentation[i];
		img = ai_out[i];


		int imageWidth = bigPage.getWidth();
		int imageHeight = bigPage.getHeight();
		img.resize(imageWidth, imageHeight);
		pixels = img;

		grayImage.setFromPixels(pixels);
		grayImage.threshold(150);

		contourFinder.findContours(grayImage, 20, (imageWidth * imageHeight) / 10, 1800, true);

		for (size_t j = 0; j < contourFinder.nBlobs; j++) {
			rect = contourFinder.blobs[j].boundingRect;
			vectorAIOut.push_back(rect);
			vectorTypeWordAIOut.push_back(attribute);
			if (!checkInterception(rect, rootNode, flag, threshold))
				interceptionRect.push_back(rect);
			flag = false;
		}
		
		drawVector = interceptionRect;

		for (size_t j = 0; j < interceptionRect.size(); j++) {
			vectorRect.push_back(interceptionRect[j]);
			vectorTypeWord.push_back(attribute);
		}

		if (i == 0)
		{
			drawVectorRed = drawVector;
			contourFinderRed = contourFinder;
		}
		if (i == 1)
		{
			drawVectorGreen = drawVector;
			contourFinderGreen = contourFinder;
		}
		if (i == 2)
		{
			drawVectorBlue = drawVector;
			contourFinderBlue = contourFinder;
		}
		interceptionRect.clear();

		contourFinder.nBlobs = 0;
		contourFinder.blobs.clear();
	}

	saveXMLFromAIOut(vectorAIOut, vectorTypeWordAIOut);
	addAttributeToXML(vectorRect, vectorTypeWord);

	vectorRect.clear();
	vectorAIOut.clear();
	vectorTypeWord.clear();
	vectorTypeWordAIOut.clear();
}

void ofApp::saveXMLFromAIOut(vector<ofRectangle>& vectorRect, vector<string>& vectorTypeWord) {

	ofxXmlSettings xml;
	xml.addTag("AI_OUT");
	xml.pushTag("AI_OUT");
	for (size_t i = 0; i < vectorRect.size(); i++) {
		xml.addTag("node");

		xml.addAttribute("node", "X", (int)vectorRect[i].x, i);
		xml.addAttribute("node", "Y", (int)vectorRect[i].y, i);
		xml.addAttribute("node", "W", (int)vectorRect[i].getWidth(), i);
		xml.addAttribute("node", "H", (int)vectorRect[i].getHeight(), i);
		xml.addAttribute("node", "typeWord", vectorTypeWord[i], i);
	}
	xml.popTag();

	ofDirectory::createDirectory(options.pathToXML, true, true);
	xml.save(options.pathToXML + "/AI_OUT.xml");

}

void ofApp::addAttributeToXML(vector<ofRectangle>& vectorRect, vector<string>& vectorTypeWord) {

	ofxXmlSettings xml;
	xml.addTag("node");
	xml.addAttribute("node", "type", "RIL_PAGE", 0);
	xml.addAttribute("node", "X", (int)rootNode->body.x, 0);
	xml.addAttribute("node", "Y", (int)rootNode->body.y, 0);
	xml.addAttribute("node", "W", (int)rootNode->body.width, 0);
	xml.addAttribute("node", "H", (int)rootNode->body.height, 0);

	xml.pushTag("node");
	rootNode->getXmlAndSetAttribute(xml, vectorRect, vectorTypeWord, thresholdTextline, thresholdWord);
	xml.popTag();

	ofDirectory::createDirectory(options.pathToXML, true, true);
	xml.save(options.pathToXML + "/res.xml");
}


void ofApp::outMapSorting() {
	/// ���������� ��������� �� 2 ������:
	//for (size_t x = 0; x < outNetA.getWidth(); x++)
	//	for (size_t y = 0; y < outNetA.getHeight(); y++)
	//	{
	//		float pixA = outNetA.getColor(x, y).r;
	//		float pixB = outNetB.getColor(x, y).r;


	//	}

	//outNetA.update();
	//outNetB.update();
}

void ofApp::startTrain() {

	std::cout << "Running on: " << (options.device == torch::kCUDA ? "CUDA" : "CPU") << std::endl;

	auto data = readInfoTextSegmentation(options);

	auto train_set = CustomDataset(data.first, options).map(torch::data::transforms::Stack<>());
	auto train_size = train_set.size().value();

	auto train_loader = torch::data::make_data_loader<torch::data::samplers::SequentialSampler>(std::move(train_set), options.train_batch_size);
	//auto train_loader =	torch::data::make_data_loader<torch::data::samplers::RandomSampler>(std::move(train_set), options.train_batch_size);

	cout << "Train dataset size === " << train_size << endl;

	auto test_set = CustomDataset(data.second, options).map(torch::data::transforms::Stack<>());
	auto test_size = test_set.size().value();

	cout << "Test dataset size === " << test_size << endl;
	cout << "Best minimal loss for saving params = " << minBestLoss << endl;
	cout << "Num epoch = " << options.iterations << endl;
	cout << "Unet scale devider = " << unetScaleDevider << endl;
	cout << "Train batch size = " << options.train_batch_size << endl;

	auto test_loader = torch::data::make_data_loader<torch::data::samplers::SequentialSampler>(std::move(test_set), options.test_batch_size);
	//auto test_loader = torch::data::make_data_loader<torch::data::samplers::RandomSampler>(std::move(test_set), options.test_batch_size);
	cout << "!" << endl;

	network.reset();
	cout << "options.num_classes: " << options.num_classes << endl;
	cout << "unetScaleDevider: " << unetScaleDevider << endl;
	network = std::make_shared<Unet>(Unet(unetScaleDevider, options.num_classes));
	cout << "111" << endl;
	network->to(options.device);

	cout << "!" << endl;

	bestLoss = minBestLoss;

	//torch::optim::SGD optimizer(network.parameters(), torch::optim::SGDOptions(0.01).momentum(0.9));
	torch::optim::Adam optimizer(network->parameters(), torch::optim::AdamOptions(options.learningRate));
	//torch::optim::Adadelta optimizer(network->parameters(), torch::optim::AdadeltaOptions(1.0));

	ofstream logEpoch("logLoss.csv");
	logEpoch.clear();
	logEpoch << "epoch;train_loss;test_loss;test_acc" << endl;;
	for (size_t i = 0; i < options.iterations; ++i) {
		int startEpochCalcTime = ofGetElapsedTimef();
		//train_loader->
		train(*network, *train_loader, optimizer, i, train_size, options);

		test(*network, *test_loader, test_size, i, options);

		std::cout << std::endl;

		if (testLossGlobal < bestLoss) {
			bestLoss = testLossGlobal;
			//torch::save(network, ofToDataPath("params/params_" + ofGetTimestampString() + ".pt"));
			torch::save(network, ofToDataPath("params/best_params_test_loss_" + ofToString(testLossGlobal) + "_ep_" + ofToString(i) + "_.pt"));
			cout << "save params with test loss = " << ofToString(testLossGlobal) << endl;
		}

		logEpoch << i << ";" << lossGlobal << ";" << testLossGlobal << ";" << testAccuracyGlobal << endl;

		cout << "calctime: " << ofGetElapsedTimef() - startEpochCalcTime << endl;
		std::cout << std::endl;
	}
	logEpoch.close();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

	if (key == '1') isDrawResultSegmentation = !isDrawResultSegmentation;



	if (key == 'd' || key == 'D') isDrawDefect = !isDrawDefect;

	//if (key == '7')outMapSorting();

	if (key == ' ') startTrain();
	//if(key == 'l' || key == 'L') loadModel(ofToDataPath("params/best_params.pt"));


	/*if (key == '1') {
		loadModel(ofToDataPath("params/best_params.pt"));

		ofDirectory dir("deffect_dataset/test/image");
		dir.listDir();
		for (size_t i = 0; i < dir.numFiles(); i++){
			predictImage(dir.getPath(i));
		}
	}*/
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
	offsetPos = ofVec2f(x, y) - lastMousePos;
	lastMousePos = ofVec2f(x, y);
	canvasPos += offsetPos;
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
	lastMousePos = ofVec2f(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {
	offsetPos = ofVec2f(0, 0);
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {
	/*if (dragInfo.files.size() == 1) {
		if(dragInfo.files[0].find(".pt") == string::npos) predictImage(dragInfo.files[0]);
		else loadModel(dragInfo.files[0]);
	}*/
}
