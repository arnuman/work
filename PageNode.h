#pragma once
#include "ofMain.h"
#include <allheaders.h> // leptonica main header for image io
#include <tesseract/baseapi.h> // tesseract main header
#include "ofxXmlSettings.h"

using namespace tesseract;

//���������� ����� ������� ��� ����� � ������ ��� ������������� ��������� 
struct NodeRect {
	
	NodeRect(ofRectangle &rect, PageIteratorLevel &type, vector<vector<BYTE>> &symbols) {
		this->rect		= rect;
		this->type		= type;
		this->symbols	= symbols;
	}

	ofRectangle				rect;
	PageIteratorLevel		type;
	vector<vector<BYTE>>	symbols;		//������ ������� ����� �������� ������������ ������� (�.�. ��������� UTF8 �� ���������� ������ �������(1-4 ������))
};

class PageNode {
public:

	PageNode(PageNode* parent, PageIteratorLevel type, ResultIterator* it, ofTrueTypeFont* font);
	PageNode(PageNode* parent, ofTrueTypeFont* font);
	~PageNode();



	string getFirstWord() {
		if (type == PageIteratorLevel::RIL_WORD)
			return content;

		for (size_t i = 0; i < children.size(); i++)		
			if(children[i]->getFirstWord() != "") return children[i]->getFirstWord();
		
		return "";
	}

	static string				pageIteratorLevelToStr(PageIteratorLevel pil);
	static PageIteratorLevel	strToPageIteratorLevel(string str);

	
	void createFromXml(ofxXmlSettings & xml, int which);
	void getXml(ofxXmlSettings & xml);
	void getXmlAndSetAttribute(ofxXmlSettings & xml, vector<ofRectangle>& interceptionRect, vector<string>& vectorTypeWordInterception, float thresholdTextline, float thresholdWord);

	void addToNodeRectsVector(vector<NodeRect>& rects);
	
	PageNode*			parent;
	ResultIterator*		it;
	PageIteratorLevel	type;
	ofRectangle			body;
	string				content;
	string				typeWord;
	vector<PageNode*>	children;
	ofTrueTypeFont*		font;
};

