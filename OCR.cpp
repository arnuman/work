#include "OCR.h"
#include <codecvt>
#include <tesseract/renderer.h>
#include <tesseract/osdetect.h>


OCR::~OCR()
{
	if (fboWords.size()) {
		for (size_t i = 0; i < fboWords.size(); i++) delete fboWords[i];
		fboWords.clear();
	}
	if (fboWordsSmall.size()) {
		for (size_t i = 0; i < fboWordsSmall.size(); i++) delete fboWordsSmall[i];
		fboWordsSmall.clear();
	}
	if (rootNode) {
		delete rootNode;
	}
}



void OCR::setup() {

	labelsFromSettingsXml.clear();
	labelsFromPage.clear();
	
	ofxXmlSettings xml;
	xml.load("settings.xml");
	
	neuroInputSize = xml.getValue("neuroInputSize", NEURO_INPUT_SIZE);

	xml.pushTag("labels");
		int numLabels = xml.getNumTags("label");
		for (size_t i = 0; i < numLabels; i++)
		{
			string str = xml.getValue("label", "", i);
			vector<string> strSp = ofSplitString(str, ":");
			if (strSp.size() > 1) {
				labelsFromSettingsXml.push_back(Label(strSp[0], ofToFloat(strSp[1])));
			}
		}
	xml.popTag();

	/*tess.SetPageSegMode(tesseract::PageSegMode::PSM_AUTO);
	tess.SetVariable("save_best_choices", "T");
	tess.SetVariable("dpi", "200");*/
	auto     numOfConfigs = 1;
	auto     **configs = new char *[numOfConfigs];
	configs[0] = (char *) "config";

	if (tess.Init(ofToDataPath("tessdata").c_str(), "rus+eng", tesseract::OEM_DEFAULT, configs, numOfConfigs, nullptr, nullptr, false)) {
		std::cout << "OCRTesseract: Could not initialize tesseract." << std::endl;
	}
	//tess.AdaptToWordStr
//	tess.SetVariable("user_defined_dpi", "70");

	//cout << "SEG MODE: " << tess.GetPageSegMode() << endl;

	tess.SetPageSegMode(tesseract::PageSegMode::PSM_AUTO_OSD);
	tess.SetVariable("save_best_choices", "T");



	//tess.SetVariable("psm", "4");
	

	//tess.SetVariable("oem", "0");
	
	rootNode    = NULL;
	currentNode = NULL; 

#ifndef _EXPORTING
	ofTrueTypeFontSettings settings("verdana.ttf", 12);
	settings.antialiased = true;
	settings.addRange(ofUnicode::Cyrillic);
	settings.addRange(ofUnicode::Space);
	settings.addRange(ofUnicode::Latin);
	settings.addRange(ofUnicode::LetterLikeSymbols);
	font.load(settings);
#endif
	
}

int OCR::getPageOrientation(const unsigned char* imagedata, int width, int height, int bytes_per_pixel, int bytes_per_line) {

	//int st = ofGetElapsedTimeMillis();

	TessBaseAPI api;
	if (api.Init(ofToDataPath("tessdata").c_str(), "osd") == 0) {
		api.SetPageSegMode(tesseract::PageSegMode::PSM_OSD_ONLY);
		api.SetImage(imagedata, width, height, bytes_per_pixel, bytes_per_line);
		OSResults osr;
		bool getOrientation = api.DetectOS(&osr);
		//cout << "calc orient time: " << ofGetElapsedTimeMillis() - st << endl;
		//cout << "result: " << osr.best_result.orientation_id * 90 << endl;
		if (osr.best_result.orientation_id != 0 && osr.best_result.orientation_id != 2) ofLogWarning() << "90 degrees rotate detected";
		api.End();

		if (getOrientation && osr.best_result.oconfidence > 1.0) {
			if(osr.best_result.orientation_id != 0 /*&& osr.best_result.oconfidence < 1.0*/) ofLogNotice() << "confidence: " << osr.best_result.oconfidence;
			return osr.best_result.orientation_id;
		}
			
		else return 0;
	}
	else ofLogError() << "failed to init tesseract osd module";
		
	return 0;
}

float OCR::getSkew(const unsigned char * imagedata, int width, int height, int bytes_per_pixel, int bytes_per_line)
{
	float returnVal = 0.0f;
	TessBaseAPI api;
	if (api.Init(ofToDataPath("tessdata").c_str(), "osd") == 0) {
		api.SetPageSegMode(tesseract::PageSegMode::PSM_AUTO_ONLY);
		api.SetImage(imagedata, width, height, bytes_per_pixel, bytes_per_line);
		
		tesseract::Orientation orientation;
		tesseract::WritingDirection direction;
		tesseract::TextlineOrder order;
		float deskew_angle;

		const tesseract::PageIterator* it = api.AnalyseLayout();
		if (it) {
			// TODO: Implement output of page segmentation, see documentation
			// ("Automatic page segmentation, but no OSD, or OCR").
			it->Orientation(&orientation, &direction, &order, &deskew_angle);
			/*tprintf(
				"Orientation: %d\nWritingDirection: %d\nTextlineOrder: %d\n"
				"Deskew angle: %.4f\n",
				orientation, direction, order, deskew_angle);*/
			//cout << "deskew_angle: " << ofRadToDeg(deskew_angle) << endl;
			returnVal = ofRadToDeg(deskew_angle);
			delete it;
		}
		else {
			returnVal = 0.0f;
		}
	}
	else ofLogError() << "failed to init tesseract osd module";

	return returnVal;
}



ofRectangle OCR::getBoundingBox(ResultIterator* it, PageIteratorLevel level) {
	int left, top, right, bottom;
	it->BoundingBox(level, &left, &top, &right, &bottom);
	return ofRectangle(glm::vec2(left,right), glm::vec2(right,bottom));
}

PageNode* OCR::getPageData() {
	return rootNode;
}


string OCR::recognizeArea(const unsigned char * imagedata, int width, int height, int bytes_per_pixel, int bytes_per_line) {
	tess.SetPageSegMode(tesseract::PageSegMode::PSM_SINGLE_BLOCK);
	tess.SetImage(imagedata, width, height, bytes_per_pixel, bytes_per_line);
	tess.Recognize(0);

	string findText = std::unique_ptr<char[]>(tess.GetUTF8Text()).get();

	/*ofstream outText(ofToDataPath("recognize_text.txt"));
	outText << findText << endl;
	outText.close();
	*/
	tess.Clear();
	return findText;
}


void OCR::reRecognizeEmptyWords(ofPixels& pix) {
	vector<PageNode*> recognizeListNodes;
	rootNode->getListReRecognizeNodes(recognizeListNodes);
	//cout << "recognizeListNodes.size(): " << recognizeListNodes.size() << endl;
	for (size_t i = 0; i < recognizeListNodes.size(); i++){
		ofRectangle roi = recognizeListNodes[i]->body;
		ofPixels pixRoi;
		pix.cropTo(pixRoi, roi.x, roi.y, roi.width, roi.height);
		recognizeListNodes[i]->content		 =  recognizeArea(pixRoi.getData(), pixRoi.getWidth(), pixRoi.getHeight(), pixRoi.getBytesPerPixel(), pixRoi.getBytesPerPixel() * pixRoi.getWidth());
		recognizeListNodes[i]->isRerecognize = false;
	}
}

void OCR::reRecognize(ofPixels& pix) {
	vector<ofRectangle> vectorRect;
	vector<ofRectangle> interceptionRect;
	ofxCvGrayscaleImage grayImage;
	ofxCvContourFinder contourFinder;
	grayImage.setFromPixels(pix.getChannel(0));
	grayImage.threshold(225);
	int imageWidth  = pix.getWidth();
	int imageHeight = pix.getHeight();

	contourFinder.findContours(grayImage, 20, (imageWidth * imageHeight) / 500, 1800, true);
	for (size_t i = 0; i < contourFinder.nBlobs; i++)
		if (contourFinder.blobs[i].boundingRect.getHeight() / (contourFinder.blobs[i].boundingRect.getWidth()) >= 0.50)
			vectorRect.push_back(contourFinder.blobs[i].boundingRect);
	bool flag = false;

	for (size_t i = 0; i < vectorRect.size(); i++)
	{
		if (checkInterception(vectorRect[i], rootNode, flag))
			interceptionRect.push_back(vectorRect[i]);
		flag = false;
	}
	vectorRect.clear();

	if (!interceptionRect.size())
		return;

	int size = interceptionRect.size();
	for (size_t i = 0; i < size; i++)
		mergeBlobs(interceptionRect);


	cout << "interceptionRect.size(): " << interceptionRect.size() << endl;

	ofImage cropImage;
	ofPixels cropPixels;
	string word;
	for (size_t i = 0; i < interceptionRect.size(); i++)
	{
		/*OCR* cropOcr = new OCR();
		cropOcr->setup();
		cropImage.setFromPixels(pix.getChannel(0));
		cropImage.crop(interceptionRect[i].getTopLeft()[0], interceptionRect[i].getTopLeft()[1], interceptionRect[i].getWidth(), interceptionRect[i].getHeight());
		cropPixels = cropImage;
		cropOcr->recognizePage(cropPixels.getData(), cropPixels.getWidth(), cropPixels.getHeight(), cropPixels.getBytesPerPixel(), cropPixels.getBytesPerPixel()*cropPixels.getWidth());*/
		//word = cropOcr->getPageData()->children[0]->children[0]->children[0]->children[0]->content;

		ofPixels pixRoi;
		pix.cropTo(pixRoi, interceptionRect[i].x, interceptionRect[i].y, interceptionRect[i].width, interceptionRect[i].height);
		word = recognizeArea(pixRoi.getData(), pixRoi.getWidth(), pixRoi.getHeight(), pixRoi.getBytesPerPixel(), pixRoi.getBytesPerPixel() * pixRoi.getWidth());
		addWordToXML(rootNode, word, interceptionRect[i]);
		//delete cropOcr;
	}
	interceptionRect.clear();
}


void OCR::addWordToXML(PageNode* page, string& arrayWords, ofRectangle& rect) {

	if (!page) return;

	for (size_t i = 0; i < page->children.size() && page->children[i]->type != RIL_WORD; i++)
	{
		if ((page->children[i]->type == RIL_BLOCK) && (i == page->children.size() - 1))
			continue;

		/// �������� ��� ������� ������:
		//if ((page->children[i]->type == RIL_BLOCK) && (page->children[i]->children[0]->children[0]->children[0]->content == ""))
		if ((page->children[i]->type == RIL_BLOCK) && (page->children[i]->getFirstWord() == ""))
			continue;

		if ((page->children[i]->body.y < rect.getCenter().y) &&
			(page->children[i]->body.y + page->children[i]->body.getHeight() > rect.getCenter().y))
			addWordToXML(page->children[i], arrayWords, rect);
		else continue;

		//if (flag)
		//{
		//	if (page->type == RIL_PAGE)
		//	{
		//		ofxXmlSettings xml;
		//		xml.addTag("node");
		//		xml.addAttribute("node", "type", "RIL_PAGE", 0);
		//		xml.addAttribute("node", "X", (int)ocr.getPageData()->body.x, 0);
		//		xml.addAttribute("node", "Y", (int)ocr.getPageData()->body.y, 0);
		//		xml.addAttribute("node", "W", (int)ocr.getPageData()->body.width, 0);
		//		xml.addAttribute("node", "H", (int)ocr.getPageData()->body.height, 0);

		//		xml.pushTag("node");
		//			page->getXml(xml);
		//		xml.popTag();

		//		ofDirectory::createDirectory(ocr.getPageFolderPath(), true, true);
		//		xml.save(ocr.getResXmlPath());
		//	
		//		//delete &vecPage;
		//	}
		//	return;
		//}
	}

	if ((page->body.y < rect.getCenter().y) &&
		(page->body.y + page->body.getHeight() > rect.getCenter().y))
	{
		int posX = 0, posY = 0;
		for (size_t i = 0; i < page->children.size(); i++)
		{
			if (rect.x > page->children[i]->body.x)
				posX++;
			if (rect.y > page->children[i]->body.y)
				posY++;
		}
		PageIteratorLevel childrenLevel = page->type;

		int iter = 0;

		PageIteratorLevel a[5] = { RIL_PAGE, RIL_BLOCK, RIL_PARA, RIL_TEXTLINE, RIL_WORD };
		for (size_t i = 0; i < 4; i++)
			if (a[i] == childrenLevel)
				iter = i;
		iter++;

		PageNode* newNode = new PageNode(page, a[iter], NULL, page->font);
		if (a[iter] == RIL_PARA)
		{
			page->children[page->children.size() - 1]->body = rect;
			page->children[page->children.size() - 1]->content = arrayWords;

			for (size_t k = page->children.size() - 1; k > posY; k--)
				page->children[k] = page->children[k - 1];
			page->children[posY] = newNode;
		}

		if (a[iter] == RIL_WORD)
		{
			page->children[page->children.size() - 1]->body = rect;
			page->children[page->children.size() - 1]->content = arrayWords;

			for (size_t k = page->children.size() - 1; k > posX; k--)
				page->children[k] = page->children[k - 1];
			page->children[posX] = newNode;
		}
		else {
			page->children[page->children.size() - 1]->body = rect;
			addWordToXML(page->children[page->children.size() - 1], arrayWords, rect);
		}

		if (page->type == RIL_PAGE)
		{
			ofxXmlSettings xml;
			xml.addTag("node");
			xml.addAttribute("node", "type", "RIL_PAGE", 0);
			xml.addAttribute("node", "X", (int)rootNode->body.x, 0);
			xml.addAttribute("node", "Y", (int)rootNode->body.y, 0);
			xml.addAttribute("node", "W", (int)rootNode->body.width, 0);
			xml.addAttribute("node", "H", (int)rootNode->body.height, 0);

			xml.pushTag("node");
				page->getXml(xml);
			xml.popTag();

			ofDirectory::createDirectory(getPageFolderPath(), true, true);
			xml.save(getResXmlPath());

		}
	}
	return;
}

bool OCR::checkInterception(ofRectangle &rect, PageNode* page, bool &flag) {
	ofRectangle  rectIntersection;
	for (size_t i = 0; i < page->children.size() && page->type != RIL_WORD; i++)
	{
		if ((page->children[i]->type == RIL_BLOCK) && (i == page->children.size() - 1)
			/// �������� ��� ������� ������:
			//&& (page->children[i]->children[0]->children[0]->children[0]->content == ""))
			&& (page->children[i]->getFirstWord() == ""))
			continue;

		if (page->children[0]->type == RIL_WORD)
		{
			rectIntersection = page->children[i]->body.getIntersection(rect);
			if (!rectIntersection.isZero())
				flag = true;

			/*if (rect.getWidth() / page->children[i]->body.getWidth() > 10 && !rectIntersection.isZero()) {
				flag = false;
			}*/

		}
		else checkInterception(rect, page->children[i], flag);

		if (flag)
			return false;
	}
	if (!flag)
		return true;
}

void OCR::mergeBlobs(vector<ofRectangle>& vectorRect)
{
	ofRectangle  rect;
	float eps = 2;
	auto iter = vectorRect.cbegin();

	for (size_t i = 0; i < vectorRect.size() - 1; i++)
	{
		for (size_t j = 1; j < vectorRect.size(); j++)
		{
			rect = vectorRect[i].getIntersection(vectorRect[j]);
			if ((!rect.isZero()) && (i != j))
			{
				vectorRect[i].growToInclude(vectorRect[j]);
				vectorRect.erase(iter + j);
				return;
			}
			if ((fabs(vectorRect[i].getCenter().y - vectorRect[j].getCenter().y) < 5) && (i != j))
			{
				if ((fabs(vectorRect[i].getBottomRight().x - vectorRect[j].getBottomLeft().x) < eps) &&
					(fabs(vectorRect[i].getTopRight().x - vectorRect[j].getTopLeft().x) < eps))
				{
					vectorRect[i].growToInclude(vectorRect[j]);
					vectorRect.erase(iter + j);
					return;
				}
			}
		}
	}
}

bool OCR::isEmptyString(string& str) {
	if (str == " ") return true;
	if (str == "")  return true;
	for (size_t i = 0; i < str.size(); i++)
		if (str[i] != ' ') return false;
	return true;
}

void OCR::recognizePage(/*PIX* pix*/const unsigned char* imagedata, int width, int height,
	int bytes_per_pixel, int bytes_per_line) {
	//tess.SetImage(pix);
	tess.SetPageSegMode(tesseract::PageSegMode::PSM_AUTO);
	tess.SetImage(imagedata, width, height, bytes_per_pixel, bytes_per_line);
	tess.Recognize(0);	

	int lcnt = 0, tcnt = 0, bcnt = 0, wcnt = 0;

	int cBlockID = -1;
	int tBlockID = -1;
	int lBlockID = -1;
	int wBlockID = -1;

	if (rootNode) {
		//rootNode->deleteChildren();
		delete rootNode;
	}

	rootNode = new PageNode(NULL, RIL_PAGE, NULL,&font);
	currentNode = rootNode;

	ofstream outTextDebug(ofToDataPath("debug.txt"));

	int numSymbols = 0;


	/*ofImage img;
	img.load("page_0_bin.png");*/



	ResultIterator* res_it = tess.GetIterator();
	while (!res_it->Empty(RIL_BLOCK)) {
		if (res_it->Empty(RIL_WORD)) {
			res_it->Next(RIL_WORD);
			continue;
		}
		if (res_it->IsAtBeginningOf(RIL_BLOCK)) {
			//cout << "START_BLOCK" << endl;
			currentNode = new PageNode(currentNode, RIL_BLOCK, res_it, &font);
		}

		if (res_it->IsAtBeginningOf(RIL_PARA)) {
			//cout << "START_PARA" << endl;			
			currentNode = new PageNode(currentNode, RIL_PARA, res_it, &font);
		}

		if (res_it->IsAtBeginningOf(RIL_TEXTLINE)) {
			currentNode = new PageNode(currentNode, RIL_TEXTLINE, res_it, &font);
			//cout << "START_TEXTLINE" << endl;
		}
		
		//cout << "START_WORD" << endl;
		PageNode* wordNode = new PageNode(currentNode, RIL_WORD, res_it, &font);
		//cout << "START_WORD2" << endl;

		string content = "";
		bool last_word_in_line = res_it->IsAtFinalElement(RIL_TEXTLINE, RIL_WORD);
		bool last_word_in_tblock = res_it->IsAtFinalElement(RIL_PARA, RIL_WORD);
		bool last_word_in_cblock = res_it->IsAtFinalElement(RIL_BLOCK, RIL_WORD);

		int left, top, right, bottom;
		res_it->BoundingBox(RIL_WORD, &left, &top, &right, &bottom);

		

		do {
			const std::unique_ptr<const char[]> grapheme(res_it->GetUTF8Text(RIL_SYMBOL));
			float r = res_it->Confidence(RIL_SYMBOL);


			int left, top, right, bottom;
			res_it->BoundingBox(RIL_SYMBOL, &left, &top, &right, &bottom);

			int hpos = left;
			int vpos = top;
			int height = bottom - top;
			int width = right - left;

			ofRectangle body = ofRectangle(glm::vec2(left, top), width, height);

			/*if (r < 99) {
				ofImage symbol;
				symbol.cropFrom(img,body.x,body.y,body.width,body.height);
				ofSaveImage(symbol.getPixels(),"bad_symbols/" + ofGetTimestampString() + ".png");
			}*/
	

			if (grapheme && grapheme[0] != 0/* && r < 98*/) {
				content += grapheme.get();
				numSymbols++;
				/*if (r < 80) {
					/*cout << "CONTENT: " << content << endl;
					cout << "R: " << r << endl;
					outTextDebug << grapheme.get() << endl;*/
					
				//}

			}
			res_it->Next(RIL_SYMBOL);
		} while (!res_it->Empty(RIL_BLOCK) && !res_it->IsAtBeginningOf(RIL_WORD));

		//cout << "word: " << content << endl;
		//wordNode->content = utf2cp(content);
		wordNode->content = content;
		if (isEmptyString(wordNode->content) && wordNode->body.width > 0 && wordNode->body.width > wordNode->body.height && (float)wordNode->body.width / wordNode->body.height < 20) {
			wordNode->isRerecognize = true;
		}
		wcnt++;

		if (last_word_in_line) {
			currentNode = currentNode->parent;			
			//cout << "END_TEXTLINE" << endl;
			lcnt++;
		}
		else {
			/// ������� �������:
			int hpos = right;
			int vpos = top;
			res_it->BoundingBox(RIL_WORD, &left, &top, &right, &bottom);
			int width = left - hpos;
		}

		if (last_word_in_tblock) {
			currentNode = currentNode->parent;
			tcnt++;
		}

		if (last_word_in_cblock) {
			currentNode = currentNode->parent;
			bcnt++;
		}
	}

	string findText = std::unique_ptr<char[]>(tess.GetUTF8Text()).get();

	ofstream outText(ofToDataPath("recognize_text.txt"));
	outText << findText << endl;
	outText.close();

	tess.Clear();

	outTextDebug.close();

	rootNode->body.width  = width;
	rootNode->body.height = height;


	//��������� � ������ ������ �� ��������
	labelsFromPage.clear();
	for (size_t i = 0; i < labelsFromSettingsXml.size(); i++)
		labelsFromPage.push_back(Label(labelsFromSettingsXml[i].tag, -1));
}



void OCR::saveToXml(bool isSavelLabel) {
	if (rootNode == NULL)return;
	ofxXmlSettings xml;
	xml.addTag("node");
	xml.addAttribute("node", "type", "RIL_PAGE", 0);
	xml.addAttribute("node", "X", (int)rootNode->body.x, 0);
	xml.addAttribute("node", "Y", (int)rootNode->body.y, 0);
	xml.addAttribute("node", "W", (int)rootNode->body.width, 0);
	xml.addAttribute("node", "H", (int)rootNode->body.height, 0);

	xml.pushTag("node");		
		//���������� ������
		/*for (size_t i = 0; i < labelsFromPage.size(); i++)
		{
			if (labelsFromPage[i].value != -1) {
				xml.addValue("label", labelsFromPage[i].tag + ":" + ofToString(labelsFromPage[i].value));
			}
		}*/
		rootNode->getXml(xml);
	xml.popTag();

	//ofDirectory::createDirectory("recognized//" + currentPdfFileName + "//" + ofToString(pageId), true,true);
	ofDirectory::createDirectory(getPageFolderPath(), true, true);
	xml.save(getResXmlPath());


	if (isSavelLabel) {
		ofxXmlSettings xmlLabel;
		for (size_t i = 0; i < labelsResult.size(); i++)
		{
			if (labelsResult[i].value != -1) {
				xmlLabel.addValue("label", labelsResult[i].tag + ":" + ofToString(labelsResult[i].value));
			}
		}
		xmlLabel.save(getLabelsXmlPath());
	}

}

void OCR::loadFromXml() {
	
	delete rootNode;

	//ofxXmlSettings xml;
	//xml.loadFile(str);
	//	
	////������ ������
	//labelsFromPage.clear();
	//xml.pushTag("node");
	//	int numLabels = xml.getNumTags("label");
	//	for (size_t i = 0; i < numLabels; i++){
	//		vector<string> val = ofSplitString(xml.getValue("label", "", i), ":");
	//		if (val.size() > 1) {
	//			labelsFromPage.push_back(Label(val[0], ofToInt(val[1])));
	//		}
	//	}
	//xml.popTag();
	//
	////��������� � ������ ������ �� ��������
	//for (size_t i = 0; i < labelsFromSettingsXml.size(); i++)
	//{
	//	bool exist = false;
	//	for (size_t j = 0; j < labelsFromPage.size(); j++)
	//	{
	//		if (labelsFromSettingsXml[i].tag == labelsFromPage[j].tag) {
	//			exist = true;
	//			break;
	//		}
	//	}

	//	if (!exist) {
	//		labelsFromPage.push_back(Label(labelsFromSettingsXml[i].tag, -1));
	//	}
	//}


	ofxXmlSettings xml;
	xml.loadFile(getLabelsXmlPath());

	//������ ������
	labelsFromPage.clear();

	int numLabels = xml.getNumTags("label");
	for (size_t i = 0; i < numLabels; i++) {
		vector<string> val = ofSplitString(xml.getValue("label", "", i), ":");
		if (val.size() > 1) {
			labelsFromPage.push_back(Label(val[0], ofToInt(val[1])));
		}
	}

	labelsResult = labelsFromSettingsXml;

	for (size_t i = 0; i < labelsFromPage.size(); i++)
	{
		bool exist = false;
		for (size_t j = 0; j < labelsResult.size(); j++)
		{
			if (labelsResult[j].tag == labelsFromPage[i].tag) {
				labelsResult[j].value = labelsFromPage[i].value;
				exist = true;
				break;
			}
		}
		if (!exist) {
			labelsResult.push_back(labelsFromPage[i]);
		}
	}


	ofxXmlSettings xmlNode;
	xmlNode.loadFile(getResXmlPath());
	currentNode = rootNode = new PageNode(NULL, &font);
	//������� ��������� ���
	rootNode->createFromXml(xmlNode,0);
};


void OCR::drawPageAsBoxes(int x, int y) {
	//ofSetColor(255, 255, 255, 255);
	//if (fboRects.isAllocated()) {
	//	fboRects.draw(x, y);
	//}
}

void OCR::drawLabels() {
	ofSetColor(0, 0, 0, 64);
	ofRect(0, 0, 300, 300);
	ofSetColor(255, 255, 255, 255);
	ofVec2f pos(10, 20);

	for (size_t i = 0; i < labelsResult.size(); i++)
	{
		font.drawString("key F" + ofToString(i + 1), pos.x, pos.y);
		font.drawString(labelsResult[i].tag, pos.x + 70, pos.y);
		if (labelsResult[i].value != -1) {
			font.drawString(ofToString(labelsResult[i].value), pos.x + 250, pos.y);
		}
		pos.y += 20;
	}

	ofSetColor(255, 255, 255, 255);
};

string OCR::getResXmlPath() {
	return "recognized\\" + pdfFolderName + "\\" + pdfFileName + "\\" + ofToString(pageId) + "\\res.xml";
}

string OCR::getLabelsXmlPath()
{
	return "recognized//" + pdfFolderName + "//" + pdfFileName + "//" + ofToString(pageId) + "//labels.xml";
}

string OCR::getPageFolderPath() {
	return "recognized//" + pdfFolderName + "//" + pdfFileName + "//" + ofToString(pageId);
}

string OCR::getPageFolderPathByPageID(unsigned int id) {
	return "recognized//" + pdfFolderName + "//" + pdfFileName + "//" + ofToString(id);
}

string OCR::getDocumentFolderPath() {
	return "recognized//" + pdfFileName;
}

ofTrueTypeFont & OCR::getFont(){
	return font;
}

bool OCR::existPage()
{
	return ofDirectory::doesDirectoryExist(getPageFolderPath());
}

void OCR::initLabels()
{
	labelsResult = labelsFromSettingsXml;

}



void OCR::updateNodeRects() {
	rects.clear();
	rootNode->addToNodeRectsVector(rects);
}

void OCR::updatePageAsBoxesFbo(bool save) {
	if (rootNode == NULL)return;

	//��������� ��������������
	updateNodeRects();

	//�������� � smallMapSize
	float scaleWidth = (float)neuroInputSize / rootNode->body.width;
	float scaleHeight = (float)neuroInputSize / rootNode->body.height;

	//������ �������
	if (fboWords.size()) {
		for (size_t i = 0; i < fboWords.size(); i++) delete fboWords[i];
		fboWords.clear();
	}
	if (fboWordsSmall.size()) {
		for (size_t i = 0; i < fboWordsSmall.size(); i++) delete fboWordsSmall[i];
		fboWordsSmall.clear();
	}

	//������� �����
	for (size_t i = 0; i < 4; i++)
	{
		fboWords.push_back(new ofPixels());
		fboWords.back()->allocate(rootNode->body.width, rootNode->body.height, ofImageType::OF_IMAGE_GRAYSCALE);
		fboWords.back()->setColor(0);

		fboWordsSmall.push_back(new ofPixels());
		fboWordsSmall.back()->allocate(neuroInputSize, neuroInputSize, ofImageType::OF_IMAGE_GRAYSCALE);
		fboWordsSmall.back()->setColor(0);
	}


	UINT64 strt = ofGetElapsedTimeMillis();
	//�������� �����
	for (size_t i = 0; i < rects.size(); i++){
		NodeRect & rect = rects[i];
		if (rect.type == PageIteratorLevel::RIL_WORD &&  rootNode->body.height*0.5 > rect.rect.height) {

			//��� ������ ������� �������� �������
			float width = rect.rect.width / (float)(rect.symbols.size());

			for (size_t j = 0; j < rect.symbols.size(); j++)
			{

				for (size_t k = 0; k < rect.symbols[j].size(); k++)
				{
					//������ ������
					int startY = rect.rect.y;
					int endY   = startY + rect.rect.height;
					for (int y = startY; y < endY; y++)
					{

						int startX	= rect.rect.x + j * width;
						int endX	= startX + width;
						for (int x = startX; x < endX; x++)
						{
							//fboWords[k]->setColor(x, y, rect.symbols[j][k]);
							fboWords[k]->getPixels()[x + y * fboWords[k]->getWidth()] = rect.symbols[j][k];
						}

					}

					//����������� ������
					startY	= floor(rect.rect.y * scaleHeight);
					endY	= ceil(startY + rect.rect.height* scaleHeight);
					for (int y = startY; y < endY; y++)
					{
						int startX = floor(rect.rect.x * scaleWidth + j * width * scaleWidth);
						int endX = ceil(startX + width * scaleWidth);
						for (int x = startX; x < endX; x++)
						{
							//fboWords[k]->setColor(x, y, rect.symbols[j][k]);
							fboWordsSmall[k]->getPixels()[x + y * fboWordsSmall[k]->getWidth()] = rect.symbols[j][k];
						}

					}

				}
			}
		}
	}
	UINT64 end = ofGetElapsedTimeMillis();

	if (!save)return;
	
	for (size_t i = 0; i < fboWords.size(); i++)	
		ofSaveImage(*fboWords[i], getPageFolderPath() +  "//Words_" + ofToString(i) + ".png", ofImageQualityType::OF_IMAGE_QUALITY_BEST);	
	for (size_t i = 0; i < fboWordsSmall.size(); i++)
		ofSaveImage(*fboWordsSmall[i], getPageFolderPath() + "//WordsSmall_" + ofToString(i) + ".png", ofImageQualityType::OF_IMAGE_QUALITY_BEST);	
}

void OCR::keyPressed(int key) {






	//������ ��������� ������
	if (labelsResult.size() > 0) {
		if (key >= ofKey::OF_KEY_F1 && key <= ofKey::OF_KEY_F12) {
			int pressedLabel = key - ofKey::OF_KEY_F1;

			if (labelsResult.size() > pressedLabel) {
				if (labelsResult[pressedLabel].value == 1)			labelsResult[pressedLabel].value = 0;
				else if (labelsResult[pressedLabel].value == -1)	labelsResult[pressedLabel].value = 1;
				else if (labelsResult[pressedLabel].value == 0)		labelsResult[pressedLabel].value = -1;
			}
			saveToXml();
		}
	}

	//���������� 
	if (key == 19) {
		cout << "save!" << endl;
		saveToXml();
		updatePageAsBoxesFbo(true);
	}

}