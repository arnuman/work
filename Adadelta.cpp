#include "Adadelta.h"

#include <torch/csrc/autograd/variable.h>
#include <torch/nn/pimpl.h>
#include <torch/optim/optimizer.h>
#include <torch/optim/serialize.h>
#include <torch/types.h>
#include <torch/utils.h>

#include <ATen/ATen.h>

#include <functional>

namespace torch {
namespace optim {
	AdadeltaOptions::AdadeltaOptions(double learning_rate) : learning_rate_(learning_rate) {}

void Adadelta::step() {
  for (size_t i = 0; i < parameters_.size(); ++i) {
    Tensor p = parameters_.at(i);

    if (!p.grad().defined()) {
      continue;
    }

    auto update = p.grad();
	if (options.weight_decay_ > 0) {
		NoGradGuard guard;
		p.grad() = p.grad() + options.weight_decay_ * p;
	}

	auto square_avg = buffer_at(square_avg_buffers, i);
	auto acc_delta = buffer_at(acc_delta_buffers, i);
	double rho = options.rho_;
	double eps = options.eps_;

	buffer_at(step_buffers, i) += 1;

	square_avg.mul_(rho).addcmul_(p.grad(), p.grad(), 1.0 - rho);
	auto std = square_avg.add(eps).sqrt_();
	auto delta = acc_delta.add(eps).sqrt_().div_(std).mul_(p.grad());
	NoGradGuard guard;
	p.add_(delta, -options.learning_rate_);
	acc_delta.mul_(rho).addcmul_(delta, delta, 1.0 - rho);
  }
}

void Adadelta::save(serialize::OutputArchive& archive) const {
	//serialize(*this, archive);
}

void Adadelta::load(serialize::InputArchive& archive) {
	//serialize(*this, archive);
}
} // namespace optim
} // namespace torch
