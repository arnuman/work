#pragma once

#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "Dataset.h"
#include "ofxOpenCv.h"
#include "Unet.h"
#include "PageNode.h"
#include <tesseract/baseapi.h> 
#include <tesseract/renderer.h>
#include <tesseract/osdetect.h>

class ofApp : public ofBaseApp{

	public:
		void setup();
		void setupSemnentationFromXML();
		void setupUnetFromXML();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);		

		void mouseScrolled(ofMouseEventArgs& mouse);
		void loadFromXml(string path);
		void updateNodeRects();
		void updatePageAsBoxesFbo(string path);
		void loadImages(string path);

		void getListFiles(std::filesystem::path rootDir);
		void computeBlackLines(ofPixels & pix, float threshold);
		void saveXMLFromAIOut(vector<ofRectangle>& vectorRect, vector<string>& vectorTypeWord);
		void addAttributeToXML(vector<ofRectangle>& vectorRect, vector<string>& vectorTypeWord);
		void loadModel(string fileName);

		void addImageToTensor(at::Tensor & t, ofPixels & inputImagePixels, int id, int imageWidth, int imageHeight);
		void predictImage();
		bool checkInterception(ofRectangle & rect, PageNode * page, bool & flag, float & threshold);		
		void startTrain();
		void outMapSorting();

		int		numPositiveInTensor(at::Tensor& t, at::Tensor& o);
		int		numPositiveInTensor(at::Tensor& t);
		float	calcAccuracy(at::Tensor& outTensor, at::Tensor& targetTensor);
		template <typename DataLoader> void train(Unet& network, DataLoader& loader, torch::optim::Optimizer& optimizer, size_t epoch, size_t data_size, Options& options);
		template <typename DataLoader> void test(Unet& network, DataLoader& loader, size_t data_size, int numEpoch, Options& options);
		void addImageToTensor(at::Tensor &t, string imagePath, int id, int imageWidth, int imageHeight);

		PageNode*	rootNode;
		PageNode*	currentNode;
		vector<NodeRect> rects;
		ofTrueTypeFont font;
				
		ofPixels words0;
		ofPixels words1;
		ofPixels smallPage;

		vector<ofPixels> ai_out;		

		ofxCvContourFinder contourFinder;
		ofxCvContourFinder contourFinderRed;
		ofxCvContourFinder contourFinderGreen;
		ofxCvContourFinder contourFinderBlue;
			

		vector<ofRectangle> drawVector;
		vector<ofRectangle> drawVectorRed;
		vector<ofRectangle> drawVectorGreen;
		vector<ofRectangle> drawVectorBlue;
		

		std::shared_ptr<Unet> network;		
		Options options;
		size_t num_classesSegmentation = 1;
		vector<string> classesNamesSegmentation;

		string						loadParamsPath;
		ofVec2f canvasPos;
		ofVec2f offsetPos;
		ofVec2f lastMousePos;

		ofImage bigPage;
		vector<ofImage> labelImages;
		vector<ofImage> segmentationMaps;

		int   unetScaleDevider;
		float binarization;
		float thresholdTextline;
		float thresholdWord;

		float bestLoss;
		float lossGlobal;
		float testLossGlobal;
		float testAccuracyGlobal;
		float trainAccuracyGlobal;
		float minBestLoss;

		bool  isDrawDefect;		
		float scaleImage;
		bool  isDrawResultSegmentation;
};

