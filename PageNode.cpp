#include "PageNode.h"



//using namespace tesseract
vector<vector<BYTE>> getSymbolIds(string &str) {
	vector<vector<BYTE>> result;
	for (int i = 0; i < str.length(); i++)
	{
		BYTE symbol	= str[i];
		
		//������, ��� ������ ����� �� �������� ������ ������� �� ���������� �������
		//������ ��� ���� ����� �� 10
		if (symbol >> 6 != 2) result.push_back(vector<BYTE>());
		result.back().push_back(symbol);
	}
	return result;
}


string PageNode::pageIteratorLevelToStr(PageIteratorLevel pil) {
	string typeStr = "";
	switch (pil)
	{
	case tesseract::RIL_BLOCK:
		typeStr = "RIL_BLOCK";
		break;
	case tesseract::RIL_PARA:
		typeStr = "RIL_PARA";
		break;
	case tesseract::RIL_TEXTLINE:
		typeStr = "RIL_TEXTLINE";
		break;
	case tesseract::RIL_WORD:
		typeStr = "RIL_WORD";
		break;
	case tesseract::RIL_SYMBOL:
		typeStr = "RIL_SYMBOL";
		break;
	case tesseract::RIL_PAGE:
		typeStr = "RIL_PAGE";
		break;
	default:
		typeStr = "RIL_UNKNOWN";
		break;
	}
	return typeStr;
}

PageIteratorLevel  PageNode::strToPageIteratorLevel(string str) {
	if (str == "RIL_BLOCK")return RIL_BLOCK;
	if (str == "RIL_PARA")return RIL_PARA;
	if (str == "RIL_TEXTLINE")return RIL_TEXTLINE;
	if (str == "RIL_WORD")return RIL_WORD;
	if (str == "RIL_SYMBOL")return RIL_SYMBOL;
	if (str == "RIL_PAGE")return RIL_PAGE;
	return RIL_UNKNOWN;
}



PageNode::PageNode(PageNode* parent, PageIteratorLevel type, ResultIterator* it, ofTrueTypeFont* font) {
	this->parent		= parent;
	this->type			= type;
	this->it			= it;	
	this->font			= font;


	if (it) {
		int left, top, right, bottom;
		it->BoundingBox(type, &left, &top, &right, &bottom);

		int hpos = left;
		int vpos = top;
		int height = bottom - top;
		int width = right - left;

		body = ofRectangle(glm::vec2(left, top), width, height);
	}

	if (parent) parent->children.push_back(this);
}

PageNode::PageNode(PageNode* parent, ofTrueTypeFont* font) {
	this->parent = parent;
	this->font = font;
}

PageNode::~PageNode()
{
	for (size_t i = 0; i < children.size(); i++)
	{
		delete children[i];
	}
}

void PageNode::createFromXml(ofxXmlSettings & xml,int which){

	int x = xml.getAttribute("node", "X", 0, which);
	int y = xml.getAttribute("node", "Y", 0, which);
	int w = xml.getAttribute("node", "W", 0, which);
	int h = xml.getAttribute("node", "H", 0, which);
	body = ofRectangle(x, y, w, h);
	
	string pilStr = xml.getAttribute("node", "type", "", which);
	this->type = PageNode::strToPageIteratorLevel(pilStr);

	if (this->type == PageIteratorLevel::RIL_WORD) {
		string  str = xml.getValue("node", "", which);
		this->content = str;
	}
	else {
		xml.pushTag("node", which);
		int numTags = xml.getNumTags("node");
		for (size_t i = 0; i < numTags; i++)
		{
			PageNode* child = new PageNode(this, font);
			child->createFromXml(xml, i);
			this->children.push_back(child);
		}
		xml.popTag();
	}
}

void PageNode::getXml(ofxXmlSettings & xml) {

	for (int i = 0; i < children.size(); i++) {
		PageNode * cld = children[i];

		xml.addTag("node");

		string typeStr = pageIteratorLevelToStr(cld->type);

		xml.addAttribute("node", "type", typeStr, i);
		xml.addAttribute("node", "X", (int)cld->body.x, i);
		xml.addAttribute("node", "Y", (int)cld->body.y, i);
		xml.addAttribute("node", "W", (int)cld->body.width, i);
		xml.addAttribute("node", "H", (int)cld->body.height, i);

		if (cld->type == RIL_WORD) {
			string contentCodes = "";
			xml.setValue("node", cld->content, i);
		}
		else {
			xml.pushTag("node", i);
				children[i]->getXml(xml);
			xml.popTag();
		}
	}
}

void PageNode::getXmlAndSetAttribute(ofxXmlSettings & xml, vector<ofRectangle>& interceptionRect, vector<string>& vectorTypeWordInterception, float thresholdTextline, float thresholdWord) {

	for (int i = 0; i < children.size(); i++) {
		PageNode * cld = children[i];

		xml.addTag("node");

		string typeStr = pageIteratorLevelToStr(cld->type);

		xml.addAttribute("node", "type", typeStr, i);

		ofRectangle  rect;
		if (cld->type == RIL_TEXTLINE)
		{	
			for (size_t j = 0; j < interceptionRect.size(); j++)
			{
				if (cld->typeWord != "")
					continue;

				rect = cld->body.getIntersection(interceptionRect[j]);

				if (cld->children[0]->getFirstWord() == "")
					continue;

				float square = cld->body.getArea();
				float squareRectIntersept = rect.getArea();
				if (square == 0 || squareRectIntersept == 0)
					continue;
				float squareIntersept = squareRectIntersept / square;
				//cout << squareIntersept << endl;
				if (squareIntersept > thresholdTextline)
				{
					cld->typeWord = vectorTypeWordInterception[j];
					xml.addAttribute("node", "typeWord", vectorTypeWordInterception[j], i);
				}
			}
		}
		xml.addAttribute("node", "X", (int)cld->body.x, i);
		xml.addAttribute("node", "Y", (int)cld->body.y, i);
		xml.addAttribute("node", "W", (int)cld->body.width, i);
		xml.addAttribute("node", "H", (int)cld->body.height, i);

		if (cld->type == RIL_WORD) {
			string contentCodes = "";
			xml.setValue("node", cld->content, i);


			for (size_t j = 0; j < interceptionRect.size() && cld->content != ""; j++)
			{
				if (cld->typeWord != "")
					continue;
				rect = cld->body.getIntersection(interceptionRect[j]);
								

				float square = cld->body.getArea();
				float squareRectIntersept = rect.getArea();
				if (square == 0 || squareRectIntersept == 0)
					continue;
				float squareIntersept = squareRectIntersept / square;
				//cout << squareIntersept << endl;
				if (squareIntersept > thresholdWord)
				{
					cld->typeWord = vectorTypeWordInterception[j];
					xml.addAttribute("node", "typeWord", vectorTypeWordInterception[j], i);
				}
			}
		}
		else {
			xml.pushTag("node", i);
			children[i]->getXmlAndSetAttribute(xml, interceptionRect, vectorTypeWordInterception, thresholdTextline, thresholdWord);
			xml.popTag();
		}
	}
}

void PageNode::addToNodeRectsVector(vector<NodeRect> &rects)
{
	if (type == PageIteratorLevel::RIL_WORD) {
		rects.push_back(NodeRect(body, type, getSymbolIds(content)));
	}
	else {
		rects.push_back(NodeRect(body, type, vector<vector<BYTE>>()));
	}
	for (size_t i = 0; i < children.size(); i++)
	{
		children[i]->addToNodeRectsVector(rects);
	}
}