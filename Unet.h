#pragma once 

#include <torch/torch.h>
#include "ofMain.h"
#include "Dataset.h"

struct Unet : torch::nn::Module {
	Unet(int n = 2, int numClasses = 1);

	torch::Tensor forward(torch::Tensor x);

	torch::nn::Conv2d conv1;
	torch::nn::Conv2d conv2;
	torch::nn::Conv2d conv3;
	torch::nn::Conv2d conv4;
	torch::nn::Conv2d conv5;
	torch::nn::Conv2d conv6;
	torch::nn::Conv2d conv7;
	torch::nn::Conv2d conv8;

	torch::nn::Conv2d convUp1;
	torch::nn::Conv2d convUp2;
	torch::nn::Conv2d convUp3;
	torch::nn::Conv2d convUp4;
	torch::nn::Conv2d convUp5;
	torch::nn::Conv2d convUp6;
	torch::nn::Conv2d convUp7;
	torch::nn::Conv2d convUp8;
	torch::nn::Conv2d convUp9;
	torch::nn::Conv2d convUp10;
	torch::nn::Conv2d convUp11;
};
